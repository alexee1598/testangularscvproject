import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {BoardComponent} from './components/board/board/board.component';
import {CreateEditSchemaComponent} from './components/create-edit-schema/create-edit-schema.component';
import {DataSetComponent} from './components/board/data-set/data-set.component';
import {LoginComponent} from './core/login/login.component';
import {OnlyNotAuthGuardService, OnlyAuthGuardService} from './core/guards';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full', canActivate: [OnlyNotAuthGuardService]},
  {path: 'board', component: BoardComponent, canActivate: [OnlyAuthGuardService]},
  {path: 'login', component: LoginComponent, canActivate: [OnlyNotAuthGuardService]},
  {path: 'data-set/:id', component: DataSetComponent, canActivate: [OnlyAuthGuardService]},
  {path: 'new-schema', component: CreateEditSchemaComponent, canActivate: [OnlyAuthGuardService]},
  {path: 'view/:id/edit', component: CreateEditSchemaComponent, canActivate: [OnlyAuthGuardService]},
  {path: 'view/:id/create', component: CreateEditSchemaComponent, canActivate: [OnlyAuthGuardService]},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}

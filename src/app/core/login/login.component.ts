import {Component, Inject} from '@angular/core';
import {Router} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import {AuthService} from '../../shared/services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  password: string;
  username: string;

  constructor(public authService: AuthService,
              @Inject(DOCUMENT) private document: Document,
              private router: Router,
  ) {
  }

  onSubmit(): void {
    this.authService.logIn(this.username, this.password)
      .subscribe(user => {
          this.router.navigateByUrl('/board').then(() =>
            this.document.location.reload()
          );
        }, error1 => {
          console.error(error1);
        }
      );
  }
}

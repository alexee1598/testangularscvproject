import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';

import {AuthService} from '../../shared/services';

@Injectable()
export class OnlyAuthGuardService implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(): boolean {
    if (!AuthService.getUser()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}

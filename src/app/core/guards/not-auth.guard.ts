import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';

import {AuthService} from '../../shared/services';

@Injectable()
export class OnlyNotAuthGuardService implements CanActivate {

  constructor(public router: Router) {
  }

  canActivate(): boolean {
    if (!AuthService.getUser()) {
      return true;
    } else {
      this.router.navigate(['/board']);
      return true;
    }
  }
}

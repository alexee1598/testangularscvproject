import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import {environment} from '../../../environments/environment';
import {AuthService} from '../../shared/services';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // const user = this.authService.userValue;
    const isLoggedIn = this.authService.userValue;
    const isApiUrl = request.url.startsWith(environment.urlBackend);
    if (isLoggedIn && isApiUrl) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${isLoggedIn}`
        }
      });
    }

    return next.handle(request);
  }
}

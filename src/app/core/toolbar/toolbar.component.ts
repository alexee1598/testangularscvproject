import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../shared/services';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  user = AuthService.getUser();

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

}

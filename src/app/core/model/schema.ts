export class Schema {
  id: number;
  updated: Date;
  title: string;
  params: string;
}

export class Dashboard {
  schemaList: Schema[];
}

export class SchemaView {
  schema: Schema;
  error: string;
  result: number;
}

export class DataSet {
  id: number;
  created: Date;
  status: string;
  file_csv: any;
}

export class DataSetDashboard {
  dataSets: DataSet[];
  result: number;
}

export interface ISimpleResponse {
  result: number;
  error: string;
}

export class LinkResponse {
  test: string;
}

export interface User {
  id: number;
  username: string;
  email: string;
  token: string;
  first_name: string;
  last_name: string;
}

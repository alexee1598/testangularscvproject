import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {BehaviorSubject, Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

import {DOCUMENT} from '@angular/common';
import {environment} from '../../../environments/environment';
import {IToken} from '../../core/model';
import {Router} from '@angular/router';
import {User} from '../../core/model';

export const createUser = (data: any) => {
  return {
    id: data.id,
    username: data.username,
    first_name: data.first_name,
    last_name: data.last_name,
  };
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userSubject$: BehaviorSubject<IToken>;
  public user$: Observable<IToken>;

  constructor(private http: HttpClient, private router: Router, @Inject(DOCUMENT) private document: Document) {
    this.userSubject$ = new BehaviorSubject<IToken>(JSON.parse(localStorage.getItem('test.auth')));
    this.user$ = this.userSubject$.asObservable();
  }

  private baseUrl = environment.production ? environment.urlBackend : 'http://localhost:8000';

  static getUser(): User {
    const accessToken = this.getAccessToken();
    if (accessToken) {
      return this.parseUserFromAccessToken(accessToken);
    }
    return undefined;
  }

  public get userValue(): any {
    return this.userSubject$.value;
  }

  static getAccessToken(): string | undefined {
    const token = JSON.parse(window.localStorage.getItem('test.auth'));
    if (token) {
      return token.access;
    }
    return undefined;
  }

  private static parseUserFromAccessToken(accessToken: string): User {
    const [, payload, ] = accessToken.split('.');
    const decoded = window.atob(payload);
    return JSON.parse(decoded);
  }

  logIn(username: string, password: string): Observable<IToken> {
    return this.http.post<IToken>(`${this.baseUrl}/login/`, {username, password}).pipe(
      tap(user => {
        localStorage.setItem('test.auth', JSON.stringify(user));
        this.userSubject$.next(user);
      })
    );
  }

  logOut(): void {
    localStorage.removeItem('test.auth');
    this.router.navigate(['login']).then(() =>
      this.document.location.reload()
    );
  }
}

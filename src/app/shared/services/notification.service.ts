import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';
import {WebSocketSubject, webSocket} from 'rxjs/webSocket';
import {share} from 'rxjs/operators';

import {AuthService} from './auth.service';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  webSocket: WebSocketSubject<any>;
  messages$: Observable<any>;

  constructor(private toastrService: ToastrService) {
    this.connect();
  }

  private connect(): Observable<any> {
    if (!this.webSocket || this.webSocket.closed) {
      const accessToken = AuthService.getAccessToken();
      this.webSocket = webSocket(`ws://localhost:8000/csv-status/?token=${accessToken}`);
      return this.messages$ = this.webSocket.pipe(share());
    }
  }

  popUpStatus(value: any): void {
    if (value.status === 'READY') {
      this.toastrService.success(``, 'CSV File is generated!');
    } else if (value.status === 'ERROR') {
      this.toastrService.error(``, 'CSV File not be generated!');
    }
  }

}

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {Dashboard, DataSetDashboard, SchemaView, ISimpleResponse, LinkResponse} from '../../core/model';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private baseUrl = environment.production ? environment.urlBackend : 'http://localhost:8000';

  constructor(private http: HttpClient) {
  }

  getSchemas(): Observable<Dashboard> {
    const headers = this.setHeaders();
    return this.http.get<Dashboard>(`${this.baseUrl}/`, { headers });
  }

  deleteSchema(id: number): Observable<ISimpleResponse> {
    const headers = this.setHeaders();
    return this.http.delete<ISimpleResponse>(`${this.baseUrl}/view/${id}/`, { headers });
  }

  saveSchema(data: any): Observable<ISimpleResponse> {
    const headers = this.setHeaders();
    return this.http.post<ISimpleResponse>(`${this.baseUrl}/create-schema/`, data, { headers });
  }

  editSchema(id: number): Observable<SchemaView> {
    const headers = this.setHeaders();
    return this.http.get<SchemaView>(`${this.baseUrl}/view/${id}/`, { headers });
  }

  updateSchema(id: number, data: any): Observable<ISimpleResponse> {
    const headers = this.setHeaders();
    return this.http.post<ISimpleResponse>(`${this.baseUrl}/view/${id}/`, data, { headers });
  }

  getDataSchema(id: number): Observable<DataSetDashboard> {
    const headers = this.setHeaders();
    return this.http.get<DataSetDashboard>(`${this.baseUrl}/data-sets-board/${id}/`, { headers });
  }

  generateData(schemaId: number, count: number): Observable<DataSetDashboard> {
    const headers = this.setHeaders();
    return this.http.get<DataSetDashboard>(`${this.baseUrl}/generate-csv/${schemaId}/${count}/`, { headers });
  }

  downloadSchema(id: number): Observable<LinkResponse> {
    const headers = this.setHeaders();
    return this.http.get<LinkResponse>(`${this.baseUrl}/get-generate-csv/${id}/`, { headers });
  }

  setHeaders(): HttpHeaders {
    const accessToken = AuthService.getAccessToken();
    return new HttpHeaders({ Authorization: `Bearer ${accessToken}` });
  }
}

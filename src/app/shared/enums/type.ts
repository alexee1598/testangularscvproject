export enum Type {
  full_name = 'Full name',
  integer = 'Integer',
  company = 'Company',
  job = 'Job',
  address = 'Address',
  domain_name = 'Domain name',
  email = 'Email',
  phone_number = 'Phone number',
  date = 'Date',
}

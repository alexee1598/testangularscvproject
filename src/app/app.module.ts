import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing';
import {BoardComponent} from './components/board/board/board.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CreateEditSchemaComponent} from './components/create-edit-schema/create-edit-schema.component';
import {DataSetComponent} from './components/board/data-set/data-set.component';
import {ErrorInterceptor} from './core/interceptors/error.interceptor';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {JwtInterceptor} from './core/interceptors/jwt.interceptor';
import {LoginComponent} from './core/login/login.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {OnlyNotAuthGuardService, OnlyAuthGuardService} from './core/guards';
import {ToastrModule} from 'ngx-toastr';
import {ToolbarComponent} from './core/toolbar/toolbar.component';

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    CreateEditSchemaComponent,
    DataSetComponent,
    LoginComponent,
    ToolbarComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    RouterModule,
    ToastrModule.forRoot()
  ],
  providers: [
    OnlyAuthGuardService,
    OnlyNotAuthGuardService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {
}

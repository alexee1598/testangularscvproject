import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {Schema} from '../../../core/model';
import {ApiService, AuthService} from '../../../shared/services';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  schemas: Schema[];

  constructor(public apiService: ApiService,
              private toastr: ToastrService,
              private authService: AuthService,
              private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.spinner.show();
    this.apiService.getSchemas().subscribe(data => {
      this.schemas = data.schemaList;
      this.spinner.hide();
    }, error => {});
  }

  trackByFn(index: number): number {
    return index;
  }

  deleteSchema(id: number): void {
    this.spinner.show();
    this.apiService.deleteSchema(id).subscribe(data => {
      if (data.result === 1) {
        this.ngOnInit();
        this.spinner.hide();
      }
    });
  }
}

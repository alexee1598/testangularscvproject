import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {NgxSpinnerService} from 'ngx-spinner';
import {DataSet} from '../../../core/model';
import {ApiService, AuthService, NotificationService} from '../../../shared/services';

@Component({
  selector: 'app-data-set',
  templateUrl: './data-set.component.html',
  styleUrls: ['./data-set.component.css']
})
export class DataSetComponent implements OnInit, OnDestroy {
  count: number;
  schemaId: number;
  dataSet: DataSet[];
  notification$: Subscription;
  blob: any;

  constructor(public apiService: ApiService,
              private route: ActivatedRoute,
              private notificationService: NotificationService,
              private authService: AuthService,
              private spinner: NgxSpinnerService,
  ) {
  }

  ngOnInit(): void {
    this.spinner.show();
    this.notification$ = this.notificationService.messages$.subscribe(
      value => {
        this.notificationService.popUpStatus(value);
        this.initData();
    });
    this.count = 200;
    this.route.params.subscribe(params => {
        this.schemaId = params.id;
        this.initData();
      }
    );
  }

  trackByFn(index: number): number {
    return index;
  }

  initData(): void {
    this.apiService.getDataSchema(this.schemaId).subscribe(data => {
      if (data.result === 1) {
        this.dataSet = data.dataSets;
      }
      this.spinner.hide();
    });
  }

  generateData(): void {
    this.spinner.show();
    this.apiService.generateData(this.schemaId, this.count).subscribe(data => {
      if (data.result === 1) {
        this.dataSet = data.dataSets;
      }
      this.spinner.hide();
    });
  }

  download(id: number): void {
    this.apiService.downloadSchema(id).subscribe(data => {
      const link = document.createElement('a');
      link.href = data.test;
      link.download = `Schema_set${id}.csv`;
      link.click();
    });
  }

  ngOnDestroy(): void {
    this.notification$.unsubscribe();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditSchemaComponent } from './create-edit-schema.component';

describe('CreateEditSchemaComponent', () => {
  let component: CreateEditSchemaComponent;
  let fixture: ComponentFixture<CreateEditSchemaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateEditSchemaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditSchemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

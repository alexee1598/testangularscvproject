import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {ApiService} from '../../shared/services';
import {Type} from '../../shared/enums/type';

@Component({
  selector: 'app-create-edit-schema',
  templateUrl: './create-edit-schema.component.html',
  styleUrls: ['./create-edit-schema.component.css']
})
export class CreateEditSchemaComponent implements OnInit {
  schemaForm: FormGroup;
  type: string[];
  edit: boolean;
  id: string;
  error: string;

  constructor(private fb: FormBuilder,
              private apiService: ApiService,
              private toastr: ToastrService,
              private router: Router,
              private spinner: NgxSpinnerService,
  ) {
  }


  ngOnInit(): void {
    this.spinner.show();
    this.type = Object.values(Type);
    this.schemaForm = this.fb.group({
      name: new FormControl('', Validators.required),
      column_separator: ',',
      string_character: '\"',
      selling_points: this.fb.array([
        this.fb.group(
          {
            column_name: new FormControl('', Validators.required),
            type: this.type[0], from: null, to: null,
            order: new FormControl(null, Validators.required)
          }
        )
      ])
    });
    this.edit = this.router.url.indexOf('edit') >= 0;

    if (this.edit) {
      this.id = this.router.url.match(new RegExp('([\\d]+)'))[1];
      this.apiService.editSchema(parseInt(this.id, 10))
        .subscribe(resp => {
            if (resp.result === 1) {
              this.populateSchemaFields(resp);
            }
            this.spinner.hide();
          }
        );
    } else {
      this.spinner.hide();
    }
  }

  get schemaFields(): FormArray {
    return this.schemaForm.get('selling_points') as FormArray;
  }

  addSellingPoint(): void {
    this.schemaFields.push(this.fb.group({column_name: '', type: '', from: null, to: null, order: null}));
  }

  deleteSellingPoint(index): void {
    this.schemaFields.removeAt(index);
  }

  populateSchemaFields(resp: any): void {
    const param = resp.schema.params;
    this.schemaForm.controls.name.setValue(param.match(new RegExp(`'name': '([\\w]+)',`))[1]);
    this.schemaForm.controls.column_separator.setValue(param.match(new RegExp(`'column_separator': '(.?)',`))[1]);
    this.schemaForm.controls.string_character.setValue(param.match(new RegExp(`'string_character': '(.?)',`))[1]);
    const par = param.split('{');
    const length = param.split('{').length - 2;
    this.deleteSellingPoint(0);
    let i = 0;
    while (i < length) {
      // tslint:disable-next-line:variable-name
      const column_name = par[i + 2].match(new RegExp((`'column_name': '([\\w ]+)',`)))[1];
      const type = par[i + 2].match(new RegExp((`'type': '([\\w ]+)',`)))[1];
      let from = null;
      let to = null;
      if (!!(par[i + 2].match(new RegExp(`'from': '?([\\d None]+)'?`)))) {
        from = par[i + 2].match(new RegExp(`'from': '?([\\d None]+)'?`))[1];
      }
      if (!!(par[i + 2].match(new RegExp(`'to': '?([\\d None]+)'?`)))) {
        to = par[i + 2].match(new RegExp(`'to': '?([\\d None]+)'?`))[1];
      }
      const order = par[i + 2].match(new RegExp(`'order': '?([\\d]+)'?`))[1];
      this.schemaFields.push(this.fb.group({column_name, type, from, to, order}));
      i += 1;
    }
  }

  saveSchema(): void {
    this.spinner.show();
    let res;
    if (this.edit) {
      res = this.apiService.updateSchema(parseInt(this.id, 10), this.schemaForm.value);
    } else {
      res = this.apiService.saveSchema(this.schemaForm.value);
    }
    res.subscribe(resp => {
      if (resp.result === 1) {
        this.spinner.hide();
        this.router.navigateByUrl('/').then(() => {});
      } else if (resp.result === 0) {
        this.error = resp.error;
        this.spinner.hide();
      }
    });
  }

  trackByFn(index: number): number {
    return index;
  }

  closeAlert(): void {
    this.error = '';
  }
}
